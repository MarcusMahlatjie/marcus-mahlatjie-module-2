import 'dart:io';

class MtnAppOfTheYearWinner{

  var appName;
  var category;
  var developer;
  var winningYear;
  var appNameCapital;

  winnerInfo(){

    print("App name: $appName");
    print("Category: $category");
    print("Developer: $developer");
    print("Winning Year: $winningYear");
  }

  appNameInCapital(){
    print("App name in capital letters: $appNameCapital");
  }

}

void main (){

  var winner = new MtnAppOfTheYearWinner();
  winner.appName = "Ambani Africa";
  winner.category = "Eductation and entertainment";
  winner.developer = "Mukhundi Lambani";
  winner.winningYear = 2021;
  winner.appNameCapital = "Ambani Africa".toUpperCase();

  print("QUESTION A");
  winner.winnerInfo();

  print("QUESTION B");
  winner.appNameInCapital();

}