import 'dart:io';


void main(List<String> arguments) {

  //VARIABLES
  String? name = '';
  String? favoriteApp = '';
  String? city = '';

  //USER INFORMATION
    print("Welcome to my MTN App Academy assessment, please enter your name");
      name = stdin.readLineSync();
    print("What is your favorite app?");
      favoriteApp = stdin.readLineSync();
    print("Which city are you from?");
      city = stdin.readLineSync();

  //OUTPUT
    print("------------------------------------------------------------------");
    print("Name: $name" );
    print("Favorite app: $favoriteApp");
    print("City: $city");

}
